package com.segas.flipper.util

interface EndlessScrollCallback{
    fun loadMore();
}