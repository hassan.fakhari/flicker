package com.segas.flipper.di.model.json

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.segas.flipper.di.model.FlickerImage

data class ResponseFlicker(

    @Expose
    @SerializedName("page")
    var page : Int,

    @Expose
    @SerializedName("pages")
    var pages : Int,

    @Expose
    @SerializedName("perpage")
    var perpage : Int,

    @Expose
    @SerializedName("total")
    var total : String ,


@Expose
@SerializedName("photo")
var photos : List<FlickerImage>



)
