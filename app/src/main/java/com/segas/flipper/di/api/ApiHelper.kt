package com.github.diver.data.api

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


object ApiHelper {



    fun create() : Retrofit {
        val httpInseperator : HttpLoggingInterceptor = HttpLoggingInterceptor();
        httpInseperator.level = HttpLoggingInterceptor.Level.BODY

        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit =
                Retrofit.Builder().addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(OkHttpClient().newBuilder().addInterceptor(httpInseperator).build())
                        .baseUrl("https://www.flickr.com/services/rest/").build()
        return retrofit

    }


}