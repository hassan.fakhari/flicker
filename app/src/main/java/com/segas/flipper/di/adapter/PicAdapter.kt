package com.segas.flipper.di.adapter

import android.animation.Animator
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.segas.flipper.R
import com.segas.flipper.di.model.FlickerImage
import com.segas.flipper.di.model.onItemClick
import eu.davidea.flipview.FlipView




class PicAdapter internal constructor(val context: Context, var onClick: onItemClick) :
    RecyclerView.Adapter<PicAdapter.ItemViewHolder>() {
    var itemList:MutableList<FlickerImage> = ArrayList()


     class ItemViewHolder internal constructor(@NonNull itemView: View) :
        ViewHolder(itemView) {
         internal  var mFlipView: FlipView = itemView.findViewById(R.id.flip_view)


    }

    override fun getItemCount(): Int {
        return itemList.size
    }
     private fun populateItemRows(viewHolder: ItemViewHolder, position: Int) {

          var pic_img : ImageView = viewHolder.mFlipView.findViewById(R.id.pic_img)
         var tvItem: TextView = viewHolder.mFlipView.findViewById(R.id.textViewItem)

         val item = itemList[position]
       tvItem.text = item.title
        var pic_url = "https://live.staticflickr.com/"+item.server+"/"+item.id+"_"+item.secret+".jpg"
        Glide.with(context).load(pic_url).into(pic_img)
         viewHolder.itemView.setOnClickListener {
             onClick.onClick(item)



         }



    }

    fun setData(promoList: MutableList<FlickerImage>)
    {
        this.itemList = promoList
    }


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        populateItemRows(holder, position)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_item, parent, false)
      return  ItemViewHolder(view)
    }
}
