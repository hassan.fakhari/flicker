package com.segas.flipper.di.model.json

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseFlickerContent(

    @Expose
    @SerializedName("photos")
    var photosData : ResponseFlicker,

@Expose
@SerializedName("stat")
var stat : String
)