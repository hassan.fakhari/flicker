package com.github.diver.data.api

 import com.segas.flipper.di.model.json.ResponseFlicker
 import com.segas.flipper.di.model.json.ResponseFlickerContent
 import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

import io.reactivex.rxjava3.core.Observable
 import retrofit2.http.Query

interface FlickerService {

 @GET("?api_key=e4206c4e11ac48f62ca6ff959478e1ab&format=json&method=flickr.photos.search&per_page=10&&nojsoncallback=1")
 fun getMainServer(@Query("page")page : Int,@Query("text")text:String):  Observable<ResponseFlickerContent?>?

}