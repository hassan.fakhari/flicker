package com.segas.flipper.di.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FlickerImage(

    @Expose
    @SerializedName("id")
    var id : String ,

    @Expose
    @SerializedName("owner")
    var owner : String ,

    @Expose
    @SerializedName("secret")
    var secret : String,

    @Expose
    @SerializedName("server")
    var server : String ,

    @Expose
    @SerializedName("from")
var from : Int,

   @Expose
   @SerializedName("title")
   var  title : String ,

@Expose
@SerializedName("ispublic")
var ispublic : Int,

)

/*
"id":"50719767861",
"owner":"137218146@N02",
"secret":"d2c324c896",
"server":"65535",
"farm":66,
"title":"Jo\u00e3o Borges",
"ispublic":1,
"isfriend":0,
"isfamily":0
 */