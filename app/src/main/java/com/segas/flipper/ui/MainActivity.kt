package com.segas.flipper.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager

import com.github.diver.data.api.ApiHelper
import com.segas.flipper.R
import com.segas.flipper.di.adapter.PicAdapter
import com.segas.flipper.di.model.FlickerImage
import com.segas.flipper.di.model.onItemClick
import com.segas.flipper.util.EndlessRecyclerView
import com.segas.flipper.util.EndlessScrollCallback

class MainActivity : AppCompatActivity() {
    lateinit var recyclerView: EndlessRecyclerView
    lateinit var recyclerViewAdapter: PicAdapter
    lateinit var rowsArrayList : MutableList<FlickerImage>
    lateinit var item_detail : TextView
     var current_page = 0
    val viewModel : MainViewModel = MainViewModel(ApiHelper,this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        item_detail= findViewById(R.id.item_detail)
        rowsArrayList = arrayListOf()
        recyclerView = findViewById(R.id.recyclerView)
        current_page++
        populateData(current_page,false)
     }






    private fun initAdapter() {
        recyclerViewAdapter = PicAdapter(this,object  : onItemClick{
            override fun onClick(item: FlickerImage) {


            }


        })

        recyclerView.layoutManager = GridLayoutManager(applicationContext,3)
        recyclerViewAdapter.setData(rowsArrayList)
        recyclerView.adapter = recyclerViewAdapter

        recyclerView.setEndlessScrollCallback (object : EndlessScrollCallback {
            override fun loadMore() {
recyclerView.blockLoading()
                current_page++
                populateData(current_page,true)
                var list = rowsArrayList
                var mutable = list.toMutableList()

                 Thread(object : Runnable {
                    override fun run() {
                        Thread.sleep(500)
                        runOnUiThread(object : Runnable {
                            override fun run() {
                                recyclerViewAdapter.setData(mutable)
                                recyclerViewAdapter.notifyDataSetChanged()

                                recyclerView.releaseBlock()
                            }
                        })
                    }
                }).start()



            }

        })
    }
    private fun populateData(page : Int,loadmore : Boolean) {

viewModel.getPicsPerPage(page,"tennis")
        viewModel.picLiveData.observe(this, Observer {
for(i in 0..it.photosData.photos.size-1){
    rowsArrayList.add(it.photosData.photos.get(i))
}
            this@MainActivity.runOnUiThread {
                item_detail.setText(it.photosData.perpage.toString() + " Pics ")
if(loadmore == false) {
    initAdapter()
}
            }


        })

    }}

