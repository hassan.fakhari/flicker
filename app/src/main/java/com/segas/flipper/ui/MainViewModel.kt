package com.segas.flipper.ui

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.diver.data.api.ApiHelper
import com.github.diver.data.api.FlickerService
import com.segas.flipper.di.model.json.ResponseFlicker
import com.segas.flipper.di.model.json.ResponseFlickerContent
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.create

class MainViewModel(var apiHelper : ApiHelper,val context : Context) : ViewModel(){



    var picLiveData = MutableLiveData<ResponseFlickerContent>()
    var picLiveError = MutableLiveData<String>()

    fun getPicsPerPage(page : Int,query : String){
        val api  = apiHelper.create().create(FlickerService::class.java) as FlickerService
api.getMainServer(page,query)?.observeOn(Schedulers.newThread())?.subscribeOn(AndroidSchedulers.mainThread())?.subscribe(
    {
        picLiveData.postValue(it)
    },
    {
        Log.w("www", "getPicsPerPage: " + it.message )
        picLiveError.postValue(it.message)
    })


    }





}